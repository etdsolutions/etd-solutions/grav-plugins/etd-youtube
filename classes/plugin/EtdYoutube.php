<?php
namespace Grav\Plugin\EtdYoutube;

use Grav\Common\Grav;
use Grav\Common\Cache;

class EtdYoutube
{
    /** @var Grav $grav */
    protected $grav;

    /**
     * @var array
     */
    protected $config;



    const CACHE_LIFETIME = 86400;

    /**
     * @param $config
     */
    public function __construct($config = [])
    {
        $this->grav = Grav::instance();
        $this->config = $config;
    }

    public function getChannelVideos($idChannel, $count=20){

        // Si on a pas d'API Key ni de Channel, on ne peut rien faire !
        if( !$this->config["api_key"] || !$idChannel ) {
            return false;
        }

        $cache = new Cache($this->grav);
        $cache_key = md5('etd_youtube_channelVideos_' . $idChannel);

        if ($data = $cache->fetch($cache_key)) {
            $results = $data;
        } else {
            //open connection
            $ch = curl_init();

            $url = "https://www.googleapis.com/youtube/v3/search?key=" . $this->config["api_key"] . "&channelId=" . $idChannel . "&part=snippet,id&order=date&maxResults=" . $count;

            //set the url, number of POST vars, POST data
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

            //So that curl_exec returns the contents of the cURL; rather than echoing it
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            // On force le certificat invalide
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

            //execute post et decode json
            $results = json_decode(curl_exec($ch));

            $cache->save($cache_key, $results, self::CACHE_LIFETIME);
        }

        return $results;
    }
}
