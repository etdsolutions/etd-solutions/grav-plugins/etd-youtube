<?php

namespace Grav\Plugin\Shortcodes;

use Grav\Common\Page\Page;
use Grav\Plugin\EtdYoutube\EtdYoutube;
use Thunder\Shortcode\Shortcode\ShortcodeInterface;
use Grav\Common\Cache;


class EtdYoutubeShortcode extends Shortcode
{
    const CACHE_LIFETIME = 86400;

    public function init()
    {



        $this->shortcode->getHandlers()->add('etd-youtube-channel', function(ShortcodeInterface $sc) {


            $cache_key = md5('etd_youtube_' . $sc->getParameter('id'));
            if($data = $this->grav['cache']->fetch($cache_key)){
                $videos = $data;
            } else {
                $etdyoutube = new EtdYoutube($this->config["plugins"]["etd-youtube"]);
                $videos = $etdyoutube->getChannelVideos($sc->getParameter('id'), $sc->getParameter('count'));
                if(isset($videos->items)){
                    $this->grav['cache']->save($cache_key, $videos, self::CACHE_LIFETIME);
                }
            }

            if (isset($videos->items)){
                $output = $this->twig->processTemplate('partials/etd-youtube-channel.html.twig', ["videos" => $videos->items]);
            } else {
                $output = '';
            }
            return $output;

        });
    }
}
